# Changelog
All notable changes to this project will be documented in this file is the author don't forget to wirte it :(.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project tries to adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1] - 2019-03-31
### Added
- Se agregaron los ejemplos del libro learning-go

[Unreleased]: https://gitlab.com/pablo.halamaj/learning-go/tree/master
